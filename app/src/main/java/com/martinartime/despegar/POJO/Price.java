package com.martinartime.despegar.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Price {

    @SerializedName("currency")
    @Expose
    private Currency currency;
    @SerializedName("final_price")
    @Expose
    private Boolean finalPrice;
    @SerializedName("base")
    @Expose
    private Integer base;
    @SerializedName("best")
    @Expose
    private Integer best;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Boolean getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Boolean finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Integer getBase() {
        return base;
    }

    public void setBase(Integer base) {
        this.base = base;
    }

    public Integer getBest() {
        return best;
    }

    public void setBest(Integer best) {
        this.best = best;
    }

}