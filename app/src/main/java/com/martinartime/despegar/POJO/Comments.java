package com.martinartime.despegar.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comments {

    @SerializedName("good")
    @Expose
    private String good;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("bad")
    @Expose
    private String bad;
    @SerializedName("title")
    @Expose
    private String title;

    public String getGood() {
        return good;
    }

    public void setGood(String good) {
        this.good = good;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBad() {
        return bad;
    }

    public void setBad(String bad) {
        this.bad = bad;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
