package com.martinartime.despegar.POJO;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.martinartime.despegar.AdministrativeDivision;

public class City {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("country")
    @Expose
    private Country country;
    @SerializedName("administrative_division")
    @Expose
    private AdministrativeDivision administrativeDivision;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public AdministrativeDivision getAdministrativeDivision() {
        return administrativeDivision;
    }

    public void setAdministrativeDivision(AdministrativeDivision administrativeDivision) {
        this.administrativeDivision = administrativeDivision;
    }

}