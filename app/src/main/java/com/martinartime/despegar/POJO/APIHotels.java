package com.martinartime.despegar.POJO;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APIHotels {

    @SerializedName("meta_data")
    @Expose
    private MetaData metaData;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
