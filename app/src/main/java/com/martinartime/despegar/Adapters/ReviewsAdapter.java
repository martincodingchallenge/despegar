package com.martinartime.despegar.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.martinartime.despegar.POJO.Amenity;
import com.martinartime.despegar.POJO.Item;
import com.martinartime.despegar.POJO.Review;
import com.martinartime.despegar.R;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static com.martinartime.despegar.Utils.Util.BREAKFST;
import static com.martinartime.despegar.Utils.Util.PARKING;
import static com.martinartime.despegar.Utils.Util.PISCN;
import static com.martinartime.despegar.Utils.Util.WIFI;

/**
 * Creado por MartinArtime el 27 de noviembre del 2019
 */
public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {

    private List<Review> reviewList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView good;
        TextView bad;
        TextView user;
        LinearLayout layGood;
        LinearLayout layBad;

        MyViewHolder(View view) {
            super(view);
            user = view.findViewById(R.id.item_user);
            good = view.findViewById(R.id.item_good);
            bad = view.findViewById(R.id.item_bad);
            layGood = view.findViewById(R.id.lay_good);
            layBad = view.findViewById(R.id.lay_bad);
        }
    }


    public ReviewsAdapter(List<Review> reviewList) {
        this.reviewList = reviewList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View reviewView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_review, parent, false);

        return new MyViewHolder(reviewView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Review review = reviewList.get(position);

        String[] name = review.getUser().getName().split(" ");
        holder.user.setText(name[0].substring(0, 1).toUpperCase() + name[0].substring(1));

        String good = review.getComments().getGood();
        if(good != null){
            holder.layGood.setVisibility(View.VISIBLE);
            holder.good.setText(good.substring(0, 1).toUpperCase() + good.substring(1));
        }
        String bad = review.getComments().getBad();
        if(bad != null){
            holder.layBad.setVisibility(View.VISIBLE);
            holder.bad.setText(bad.substring(0, 1).toUpperCase() + bad.substring(1));
        }

    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

}