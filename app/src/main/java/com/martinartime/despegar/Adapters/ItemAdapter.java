package com.martinartime.despegar.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.martinartime.despegar.POJO.Amenity;
import com.martinartime.despegar.POJO.Item;
import com.martinartime.despegar.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.martinartime.despegar.Utils.Util.BREAKFST;
import static com.martinartime.despegar.Utils.Util.PARKING;
import static com.martinartime.despegar.Utils.Util.PISCN;
import static com.martinartime.despegar.Utils.Util.WIFI;

/**
 * Adapter to set each cardview of every hotel fetched from the api in the search fragment
 */
public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.MyViewHolder> {

    private Context context;
    private List<Item> itemsList;
    private ItemAdapterListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        TextView address;
        TextView rating;
        TextView price;
        ImageView thumbnail;
        ImageView star1;
        ImageView star2;
        ImageView star3;
        ImageView star4;
        ImageView star5;
        ImageView air;
        ImageView pool;
        ImageView wifi;
        ImageView gym;
        ImageView parking;
        ImageView breakfast;

        MyViewHolder(View view) {
            super(view);
            thumbnail = view.findViewById(R.id.imagen_item);
            name = view.findViewById(R.id.name_item);
            address = view.findViewById(R.id.address_item);
            rating = view.findViewById(R.id.rating_item);
            star1 = view.findViewById(R.id.img_star1);
            star2 = view.findViewById(R.id.img_star2);
            star3 = view.findViewById(R.id.img_star3);
            star4 = view.findViewById(R.id.img_star4);
            star5 = view.findViewById(R.id.img_star5);
            air = view.findViewById(R.id.img_air);
            wifi = view.findViewById(R.id.img_wifi);
            gym = view.findViewById(R.id.img_gym);
            parking = view.findViewById(R.id.img_parking);
            breakfast = view.findViewById(R.id.img_break);
            pool = view.findViewById(R.id.img_pool);
            price = view.findViewById(R.id.price_item);

            view.setOnClickListener(view1 ->
                    listener.onItemSelected(itemsList.get(getAdapterPosition())));
        }
    }


    public ItemAdapter(Context context, List<Item> itemsList, ItemAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.itemsList = itemsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_menu, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Item item = itemsList.get(position);

        Glide.with(context)
                .load(item.getMainPicture())
                .error(R.drawable.ic_logo)
                .centerCrop()
                .into(holder.thumbnail);

        holder.name.setText(item.getName());
        holder.address.setText(item.getAddress());
        holder.rating.setText(String.format("%.1f", item.getRating()));

        Integer stars = item.getStars();

        if(stars>=1){
            holder.star1.setVisibility(View.VISIBLE);
            if(stars>=2){
                holder.star2.setVisibility(View.VISIBLE);
                if(stars>=3){
                    holder.star3.setVisibility(View.VISIBLE);
                    if(stars>=4){
                        holder.star4.setVisibility(View.VISIBLE);
                        if(stars>=5){
                            holder.star5.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }

        for (Amenity am : item.getAmenities()) {
            switch (am.getId()){
                case WIFI:
                    holder.wifi.setVisibility(View.VISIBLE);
                    break;
                case PISCN:
                    holder.pool.setVisibility(View.VISIBLE);
                    break;
                case BREAKFST:
                    holder.breakfast.setVisibility(View.VISIBLE);
                    break;
                case PARKING:
                    holder.parking.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
        }
        
        int price = item.getPrice().getBase();

        NumberFormat formatter = NumberFormat.getInstance(new Locale("da", "DK"));
        String priceToShow = formatter.format(price);
        holder.price.setText("$ "+priceToShow);

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }


    public interface ItemAdapterListener {
        void onItemSelected(Item item);
    }
}