package com.martinartime.despegar.Utils;

/**
 * Creado por MartinArtime el 30 de noviembre del 2019
 */
public class Util {

    public static final String URL_BASE = "http://private-a2ba2-jovenesdealtovuelo.apiary-mock.com/";
    public static final String BUNDLE_NAME = "bundle_name";
    public static final String LIST = "hotel_list";
    public static final String ITEM = "hotel_item";
    public static final String WIFI = "WIFI";
    public static final String PISCN = "PISCN";
    public static final String BREAKFST = "BREAKFST";
    public static final String PARKING = "PARKING";

}
