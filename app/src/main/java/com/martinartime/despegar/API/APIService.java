package com.martinartime.despegar.API;

import com.martinartime.despegar.POJO.APIHotels;
import com.martinartime.despegar.POJO.APIItemDetails;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Interface that contains the signature methods with all the posisble requests to the server
 *
 *  Creado por MartinArtime el 30 de noviembre del 2019
 */
public interface APIService {

    /**
     * Query all hotels signature
     */
    @GET("hotels")
    Flowable<APIHotels> searchHotels(@Header("Content-Type") String content_type);

    /**
     * Item details searching signature
     */
    @GET("hotels/{hotel_id}")
    Flowable<APIItemDetails> searchItem(@Path("hotel_id") String hotel_id);

}

