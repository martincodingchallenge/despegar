package com.martinartime.despegar;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.martinartime.despegar.API.APIService;
import com.martinartime.despegar.API.RetrofitClient;

import static com.martinartime.despegar.Utils.Util.URL_BASE;

/**
 * Application instance which has some objects relevant to all the app lifecycle
 */
public class App extends Application {

    public static Gson gson;
    public static APIService apiService;

    private static App instance;

    public static App getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static APIService getAPIService() {
        return apiService;
    }

    public static Gson getGson() {
        return gson;
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();

        gson = new Gson();
        apiService = RetrofitClient.getClient(URL_BASE).create(APIService.class);
    }

}
