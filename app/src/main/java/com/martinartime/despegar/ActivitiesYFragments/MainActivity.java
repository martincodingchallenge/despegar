package com.martinartime.despegar.ActivitiesYFragments;

import android.os.Bundle;
import android.view.Menu;

import com.google.gson.reflect.TypeToken;
import com.martinartime.despegar.Adapters.ItemAdapter;
import com.martinartime.despegar.POJO.Item;
import com.martinartime.despegar.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.ButterKnife;

import static com.martinartime.despegar.App.getGson;
import static com.martinartime.despegar.Utils.Util.BUNDLE_NAME;
import static com.martinartime.despegar.Utils.Util.LIST;

/**
 *  Main Activity in charge of displaying and managing all fragments of the app
 */
public class MainActivity extends BaseActivity implements ItemAdapter.ItemAdapterListener {

    public Item itemSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        Type listType = TypeToken.getParameterized(ArrayList.class, Item.class).getType();
        Bundle b = Objects.requireNonNull(getIntent().getExtras()).getBundle(BUNDLE_NAME);

        if(b!=null){
            ArrayList<Item> items = getGson().fromJson(b.getString(LIST), listType);
            itemAdapter = new ItemAdapter(this, items, this);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        changeFragment(fgSearch);

        itemAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemSelected(Item item) {
        // If user clicked on an item in the search/results fragment we go to the details fragment
        itemSelected = item;
        changeFragment(fgDetails);
    }

}
