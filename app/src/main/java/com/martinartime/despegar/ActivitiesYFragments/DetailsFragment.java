package com.martinartime.despegar.ActivitiesYFragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.martinartime.despegar.Adapters.ReviewsAdapter;
import com.martinartime.despegar.App;
import com.martinartime.despegar.POJO.APIItemDetails;
import com.martinartime.despegar.POJO.Amenity;
import com.martinartime.despegar.POJO.Item;
import com.martinartime.despegar.POJO.Review;
import com.martinartime.despegar.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.martinartime.despegar.App.apiService;
import static com.martinartime.despegar.App.gson;
import static com.martinartime.despegar.Utils.Util.BREAKFST;
import static com.martinartime.despegar.Utils.Util.BUNDLE_NAME;
import static com.martinartime.despegar.Utils.Util.ITEM;
import static com.martinartime.despegar.Utils.Util.PARKING;
import static com.martinartime.despegar.Utils.Util.PISCN;
import static com.martinartime.despegar.Utils.Util.WIFI;

/**
 * Fragment that displays a hotel's detail page
 */
public class DetailsFragment extends Fragment {

    @BindView(R.id.name_item) TextView name;
    @BindView(R.id.address_item) TextView address;
    @BindView(R.id.rating_item) TextView rating;
    @BindView(R.id.price_item) TextView price;
    @BindView(R.id.imagen_item) ImageButton thumbnail;
    @BindView(R.id.img_star1) ImageView star1;
    @BindView(R.id.img_star2) ImageView star2;
    @BindView(R.id.img_star3) ImageView star3;
    @BindView(R.id.img_star4) ImageView star4;
    @BindView(R.id.img_star5) ImageView star5;
    @BindView(R.id.img_air) ImageView air;
    @BindView(R.id.img_pool) ImageView pool;
    @BindView(R.id.img_wifi) ImageView wifi;
    @BindView(R.id.img_gym) ImageView gym;
    @BindView(R.id.img_parking) ImageView parking;
    @BindView(R.id.img_break) ImageView breakfast;

    @BindView(R.id.txt_no_reviews) TextView noReviews;
    @BindView(R.id.recycler_view_comments) RecyclerView frame;

    private List<Review> reviewsList = new ArrayList<>();
    private Item item;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, viewGroup, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MainActivity parentActivity = (MainActivity) getActivity();

        // If parent activity is not null I can fill all fields of the result selected
        if(parentActivity !=null){
            item = parentActivity.itemSelected;

            // I make an async search to fetch all the information regarding the hotel selected
            parentActivity.compositeDisposable.add(apiService.searchItem(item.getId())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));

            // Set all fields
            Glide.with(App.getContext())
                    .load(item.getMainPicture())
                    .error(R.drawable.ic_logo)
                    .centerCrop()
                    .into(thumbnail);

            name.setText(item.getName());
            address.setText(item.getAddress());
            rating.setText(String.format("%.1f", item.getRating()));

            Integer stars = item.getStars();

            if(stars>=1){
                star1.setVisibility(View.VISIBLE);
                if(stars>=2){
                    star2.setVisibility(View.VISIBLE);
                    if(stars>=3){
                        star3.setVisibility(View.VISIBLE);
                        if(stars>=4){
                            star4.setVisibility(View.VISIBLE);
                            if(stars>=5){
                                star5.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
            }

            for (Amenity am : item.getAmenities()) {
                switch (am.getId()){
                    case WIFI:
                        wifi.setVisibility(View.VISIBLE);
                        break;
                    case PISCN:
                        pool.setVisibility(View.VISIBLE);
                        break;
                    case BREAKFST:
                        breakfast.setVisibility(View.VISIBLE);
                        break;
                    case PARKING:
                        parking.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }
            }

            int basePrice = item.getPrice().getBase();

            NumberFormat formatter = NumberFormat.getInstance(new Locale("da", "DK"));
            String priceToShow = formatter.format(basePrice);
            price.setText("$ "+priceToShow);

        }

        thumbnail.setOnClickListener(view -> {
            Intent intent = new Intent(parentActivity, FullScreenImageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(ITEM, gson.toJson(item));
            intent.putExtra(BUNDLE_NAME, bundle);
            startActivity(intent);
        });
    }

    /**
      * Handle response of API call to fetch the rest of information of the hotel
     */
    private void handleResponse(APIItemDetails details) {

        if(details!=null){
            if(details.getHotel()!=null){
                if(details.getHotel().getReviews()==null){
                    noReviews.setVisibility(View.VISIBLE);
                } else {
                    reviewsList.addAll(details.getHotel().getReviews());
                }
            }
        }

        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(reviewsList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(App.getContext());
        frame.setLayoutManager(mLayoutManager);
        frame.setItemAnimator(new DefaultItemAnimator());
        frame.setAdapter(reviewsAdapter);

        reviewsAdapter.notifyDataSetChanged();
    }

    /**
     * Handle error of api call to fetch hotel's information
     */
    private void handleError(Throwable error) {
        noReviews.setVisibility(View.VISIBLE);
        Log.e("TAG", "handleError: "+ error.getLocalizedMessage());
        //Toast.makeText(getActivity(), "Error! No se pudo buscar correctamente. "+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

}
