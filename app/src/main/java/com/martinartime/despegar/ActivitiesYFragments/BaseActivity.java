package com.martinartime.despegar.ActivitiesYFragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.martinartime.despegar.API.APIService;
import com.martinartime.despegar.Adapters.ItemAdapter;
import com.martinartime.despegar.POJO.APIHotels;
import com.martinartime.despegar.POJO.Item;
import com.martinartime.despegar.R;
import com.martinartime.despegar.Utils.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.martinartime.despegar.ActivitiesYFragments.SplashScreen.APP_JSON;
import static com.martinartime.despegar.App.getAPIService;
import static com.martinartime.despegar.App.gson;
import static com.martinartime.despegar.Utils.Util.BUNDLE_NAME;
import static com.martinartime.despegar.Utils.Util.LIST;

/**
 * Activity which serves as a base for all others in the app
 */
@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    public CompositeDisposable compositeDisposable = new CompositeDisposable();
    public APIService apiService = getAPIService();
    public List<Item> resultList = new ArrayList<>();
    public ItemAdapter itemAdapter;

    SearchFragment fgSearch = new SearchFragment();
    DetailsFragment fgDetails = new DetailsFragment();

    /**
     * Method that changes the fragment replacing the one currently displaying
     * @param fragment that will be now displayed
     */
    public void changeFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.contentFragment, fragment);
        transaction.commit();
    }

    /**
     * If the api call is successful it handles the response
     * @param response: search query result to parse
     */
    public void handlePositiveResponse(APIHotels response) {

        // Add all results to list
        resultList.clear();
        resultList.addAll(response.getItems());

        Intent i = new Intent(this, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(LIST, gson.toJson(resultList));
        i.putExtra(BUNDLE_NAME, bundle);
        startActivity(i, bundle);

    }

    /**
     * If api call was not successful, we manage the error
     * @param error: what happened?!?!
     */
    public void handleNegativeResponse(Throwable error) {
        error.printStackTrace();
        Toast.makeText(this, "Error! No se pudo buscar correctamente.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // We clear the composite disposable to avoid a memory leak
        compositeDisposable.clear();
    }

}
