package com.martinartime.despegar.ActivitiesYFragments;

import android.os.Bundle;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.martinartime.despegar.App;
import com.martinartime.despegar.POJO.Item;
import com.martinartime.despegar.R;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.martinartime.despegar.App.getGson;
import static com.martinartime.despegar.Utils.Util.BUNDLE_NAME;
import static com.martinartime.despegar.Utils.Util.ITEM;

public class FullScreenImageActivity extends AppCompatActivity {

    @BindView(R.id.img_fullscreen) ImageView imgFullscreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_image);

        ButterKnife.bind(this);

        Item item;

        Bundle b = Objects.requireNonNull(getIntent().getExtras()).getBundle(BUNDLE_NAME);
        if(b != null){
            item = getGson().fromJson(b.getString(ITEM), Item.class);

            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

            Glide.with(App.getContext())
                    .load(item.getMainPicture())
                    .error(R.drawable.ic_logo)
                    .fitCenter()
                    .into(imgFullscreen);
        }

        imgFullscreen.setOnClickListener(view -> finish());
    }
}
