package com.martinartime.despegar.ActivitiesYFragments;

import android.os.Bundle;

import com.martinartime.despegar.R;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Splash screen used to make some time while the hotels are fetched from the API
 */
public class SplashScreen extends BaseActivity {

    public static final String APP_JSON = "text/html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Add to a composite disposable observable
        compositeDisposable.add(apiService.searchHotels(APP_JSON)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handlePositiveResponse,this::handleNegativeResponse));

    }
}
