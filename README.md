# Despegar Coding Challenge

Esta es la resolución del coding challenge propuesto por Despegar.

Decisiones que tome respecto a la arquitectura de la app:
- Intenté hacerla similar a la app real de Despegar de manera que fuera fácil de identificar.
- Me pareció interesante probar encarar la app con el uso de fragmentos en vez de muchas activities (que son más pesadas) y dado que en mi idea había bastante dinamismo entre vistas, el uso de fragments que se puedan intercambiar rápidamente cumplía con el requisito.
- Use el paradigma reactivo para poder manejar las peticiones a la API de forma asincrónica y no bloquear el UI Thread.
- Utilice la 'Splash Screen' como herramienta visual para que el usuario sepa que está buscando lo que el quiere mientras se cargan los hoteles con la petición a la API por detras.

Componentes externos utilizados:
- Butterknife: para bindeo de vistas.
- Retrofit: para comunicación con servidor backend.
- Gson: para (de)serialización de payloads en requests/responses a la API.
- rxJava/rxAndroid: para manejo de asincronismo en peticiones a la API.
- Glide: para manejo de fotos en la app.

Cosas que mejoraría en una futura versión:
- Agregar algunas fotos "dummy" de manera que se vea mejor estéticamente la app para todas los hoteles para los que la api no tiene foto (Solo uno tiene)
- Usar view binding en vez de Butterknife (que luego será deprecado).
- Cambiar algunas cuestiones de navegabilidad.
- Agregar más datos de los productos en detalle.
- Cambiar a Kotlin para ver cuanto simplifica la base de código.
- Mejorar la forma de mostrar las imagenes en la pantalla de detalle.

